package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/widget"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

const myFormStart = `<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Döntési preferencia kérdőív</title>

    <style>
    body, html {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        width: 100%;
        height: 100%;
        font-size: 1.4rem;
        font-family: system-ui, sans-serif;
        -webkit-font-smoothing: antialiased;
        background-color: #EEE;
    }

    fieldset {
        margin: 3.0rem;
        padding: 2rem;
        box-sizing: border-box;
        display: block;
        border: none;
        border: solid 1px #CCC;
        min-width: 0;
        background-color: #FFF;
    }

    fieldset button {
        margin: 0;
        padding: 0;
        width: 100%;
        float: left;
        display: table;
        font-size: 1.4rem;
        line-height: 140%;
        font-weight: 600;
        color: #333;
    }

    fieldset button + * {
        clear: both;
    }

    body:not(:-moz-handler-blocked) fieldset {
        display: table-cell;
    }

    .toggle {
        margin: 0rem;
        box-sizing: border-box;
        font-size: 0;
        display: flex;
        flex-flow: row nowrap;
        justify-content: flex-start;
        align-items: stretch;
    }

    .toggle input {
        width: 0;
        height: 0;
        position: absolute;
        left: -9999px;
    }

    .toggle input + label {
        margin: 0;
        padding: 0.75rem;
        box-sizing: border-box;
        position: relative;
        display: inline-block;
        border: solid 1px #DDD;
        background-color: #FFF;
        font-size: 1.4rem;
        line-height: 140%;
        font-weight: 600;
        text-align: center;
        box-shadow: 0 0 0 rgba(255, 255, 255, 0);
        transition: border-color 0.15s ease-out, color 0.25s ease-out, background-color 0.15s ease-out, box-shadow 0.15s ease-out;
        flex: 0 0 33.3%; display: flex; justify-content: center; align-items: center;
    }

    .toggle input + label:first-of-type {
        border-radius: 6px 0 0 6px;
        border-right: none;
    }

    .toggle input + label:last-of-type {
        border-radius: 0 6px 6px 0;
        border-left: none;
    }

    .toggle input:hover + label {
        border-color: #213140;
    }

    .toggle input:checked + label {
        background-color: #4B9DEA;
        color: #FFF;
        box-shadow: 0 0 10px rgba(102, 179, 251, 0.5);
        border-color: #4B9DEA;
        z-index: 1;
    }

    .toggle input:focus + label {
        outline: dotted 1px #CCC;
        outline-offset: 0.45rem;
    }

    @media (max-width: 800px) {
        .toggle input + label {
            padding: 0.75rem;
            flex: 0 0 33.3%;
            display: flex;
            justify-content: center;
            align-items: center;
        }
    }

    </style>
</head>
<body>
    <form method="POST">`

type middle_html_params struct {
	radio_group_name int
	element_id_A     string
	choice_A_text    string
	element_id_B     string
	choice_B_text    string
	element_id_C     string
	choice_C_text    string
}

func generate_middle_html(
	radio_group_name int,
	element_id_A string,
	choice_A_text string,
	element_id_B string,
	choice_B_text string,
	element_id_C string,
	choice_C_text string) string {

	string_builder := strings.Builder{}

	string_builder.WriteString(`
        <fieldset>
            <div class="toggle">
                <input type="radio" name="`)

	string_builder.WriteString(strconv.Itoa(radio_group_name)) // group name

	string_builder.WriteString(`" value="true" id="`)

	string_builder.WriteString(element_id_A) //HTML element ID A

	string_builder.WriteString(`">
                <label for="`)

	string_builder.WriteString(element_id_A) //label for HTML element ID A

	string_builder.WriteString(`">`)

	string_builder.WriteString(choice_A_text) // text for HTML label for element A

	string_builder.WriteString(`</label>
                <input type="radio" name="`)

	string_builder.WriteString(strconv.Itoa(radio_group_name)) // group name

	string_builder.WriteString(`" value="null" id="`)

	string_builder.WriteString(element_id_B) //HTML element ID B

	string_builder.WriteString(`" checked>
                <label for="`)

	string_builder.WriteString(element_id_B) //label for HTML element ID B

	string_builder.WriteString(`">`)

	string_builder.WriteString(choice_B_text) // Choice 2 text

	string_builder.WriteString(`</label>
                <input type="radio" name="`)

	string_builder.WriteString(strconv.Itoa(radio_group_name)) // group name

	string_builder.WriteString(`" value="false" id="`)

	string_builder.WriteString(element_id_C) //HTML element ID C

	string_builder.WriteString(`">
                <label for="`)

	string_builder.WriteString(element_id_C) // text for HTML label for element C

	string_builder.WriteString(`">`)

	string_builder.WriteString(choice_C_text) // Choice 2 text

	string_builder.WriteString(`</label>
            </div>
        </fieldset>`)

	return string_builder.String()
}

const myFormEnd = `
            <fieldset>
                <button>Küldés</button>
            </fieldset>
    </form>
</body>
</html>
`

var the_html_code string

type compared_preference struct {
	first      string
	second     string
	third      string
	first_wins bool
}

var middle_html_params_to_randomize []middle_html_params

func main() {

	os.Setenv("FYNE_SCALE", "1.4")

	my_fyne_program := app.New()

	main_window := my_fyne_program.NewWindow("ZRDSS")
	main_window.SetMaster()
	const main_window_width float32 = 400
	const main_window_height float32 = 500
	main_window.Resize(fyne.NewSize(main_window_width, main_window_height))
	main_window.SetFixedSize(true)
	main_window.CenterOnScreen()

	input_list_window := my_fyne_program.NewWindow("Döntési szempontok megadása")
	input_list_window.Resize(fyne.NewSize(main_window_width, main_window_height))
	input_list_window.SetFixedSize(true)
	input_list_window.CenterOnScreen()
	input_list_window.Hide()

	var btn_get_dontesi_pref *widget.Button
	var btn_generate_survey *widget.Button
	var btn_start_server *widget.Button
	var btn_get_mail_addresses *widget.Button
	var btn_smtp_login *widget.Button
	var btn_send_mails *widget.Button
	var btn_aggregate_results *widget.Button
	var btn_save_settings *widget.Button
	var btn_load_settings *widget.Button
	var btn_save_results *widget.Button
	var btn_load_results *widget.Button

	dontesi_szempontok := ""
	dontesi_array := strings.Split(dontesi_szempontok, "\n")
	dontesi_array_len := len(dontesi_array)
	dontesi_szempontok_lista_okay := false

	multiline_input := widget.NewMultiLineEntry()

	btn_accept_multiline_input := widget.NewButton("input!", func() {
		dontesi_szempontok = multiline_input.Text
		dontesi_array = strings.Split(dontesi_szempontok, "\n")
		dontesi_array_len = len(dontesi_array)
		dontesi_szempontok_lista_okay = (!(dontesi_szempontok == "") && dontesi_array_len > 2)

		if dontesi_szempontok_lista_okay {
			input_list_window.Hide()
			main_window.Show()
			btn_generate_survey.Enable()
			btn_get_dontesi_pref.Disable()
		} else {
			input_list_window.Show()
			dialog.ShowInformation(
				"Hiba!",
				"Kérem legalább 3 szempontot adjon meg.",
				input_list_window,
			)
		}
	})

	mylayout_for_multiline_inp := container.NewWithoutLayout(multiline_input, btn_accept_multiline_input)

	const input_box_width = main_window_width * 0.93
	const input_box_height = main_window_height * 0.92

	const accept_btn_width = input_box_width * 0.6
	const accept_btn_height = (main_window_height - input_box_height) * 0.63

	multiline_input.Resize(fyne.NewSize(input_box_width, input_box_height))
	btn_accept_multiline_input.Resize(fyne.NewSize(accept_btn_width, accept_btn_height))

	accept_btn_pos_x := (main_window_width - btn_accept_multiline_input.Size().Width) * 0.5
	accept_btn_pos_y := main_window_height - btn_accept_multiline_input.Size().Height - ((main_window_height - input_box_height) * 0.25)

	accept_btn_pos := fyne.NewPos(accept_btn_pos_x, accept_btn_pos_y)

	input_box_pos_x := (main_window_width - multiline_input.Size().Width) * 0.33
	const input_box_pos_y float32 = 0

	input_box_pos := fyne.NewPos(input_box_pos_x, input_box_pos_y)

	btn_accept_multiline_input.Move(accept_btn_pos)
	multiline_input.Move(input_box_pos)

	input_list_window.SetContent(mylayout_for_multiline_inp)

	btn_aggregate_results = widget.NewButton("7. Eredmények összesítése és kiértékelése", func() {
		dialog.ShowInformation(
			"results",
			"ezzel a gombbal kezdheti majd el az eredmények aggregáltását, és értékelését",
			main_window,
		)
	})
	btn_aggregate_results.Disable()

	btn_send_mails = widget.NewButton("6. Linkek elküldése az email címekre", func() {
		dialog.ShowInformation(
			"send emails",
			"ezzel a gombbal fogja kiküldeni az emaileket",
			main_window,
		)
		btn_aggregate_results.Enable()
		btn_send_mails.Disable()
	})
	btn_send_mails.Disable()

	btn_smtp_login = widget.NewButton("5. SMTP Bejelentkezés", func() {
		dialog.ShowInformation(
			"SMTP login",
			"itt lehet majd bejelentkezni egy email fiókba",
			main_window,
		)
		btn_send_mails.Enable()
		btn_smtp_login.Disable()
	})
	btn_smtp_login.Disable()

	btn_get_mail_addresses = widget.NewButton("4. Email címek megadása", func() {
		dialog.ShowInformation(
			"email címek listája",
			"ide írhatja majd be az email címeket",
			main_window,
		)
		btn_smtp_login.Enable()
		btn_get_mail_addresses.Disable()
	})
	btn_get_mail_addresses.Disable()

	btn_start_server = widget.NewButton("3. Szerver elindítása", func() {
		dialog.ShowInformation(
			"server start",
			"ez indítja a szervert",
			main_window,
		)

		myserver := chi.NewRouter()
		myserver.Use(middleware.RequestID)
		myserver.Use(middleware.Logger)
		myserver.Use(middleware.Recoverer)

		myserver.Get("/", func(server_writer http.ResponseWriter, client *http.Request) {

			if client.URL.Path != "/" {
				http.Error(server_writer, "404 not found.", http.StatusNotFound)
				return
			}

			server_writer.Header().Set("Content-Type", "text/html; charset=utf-8")
			fmt.Fprint(server_writer, the_html_code)

		})

		myserver.Post("/", func(server_writer http.ResponseWriter, client *http.Request) {

			if client.URL.Path != "/" {
				http.Error(server_writer, "404 not found.", http.StatusNotFound)
				return
			}

			if err := client.ParseForm(); err != nil {

				fmt.Fprintf(server_writer, "ParseForm() err: %v", err)

			} else {
				//fmt.Fprintln(server_writer, "client.PostFrom =", client.PostForm)

				for key, value := range client.Form {
					//log.Println("KEY =", key, " VALUE =", value)

					key_index, _ := strconv.Atoi(strings.TrimSpace(key))

					//ok_value := strings.Trim(strings.Trim(strings.TrimSpace(value), "["), "]")

					//log.Println(key_index)
					//log.Println(middle_html_params_to_randomize[key_index])

					bool_value, _ := strconv.ParseBool(value[0])

					log.Println(key_index, "=", bool_value)

					// TODO: unrandomzie, store, and output results in table
				}
			}

			//first := client.FormValue("1")
			//second := client.FormValue("2")
			//third := client.FormValue("3")
		})

		go http.ListenAndServe(":3333", myserver)

		btn_get_mail_addresses.Enable()
		btn_start_server.Disable()
	})
	btn_start_server.Disable()

	btn_generate_survey = widget.NewButton("2. Kérdőív generálása", func() {

		if dontesi_szempontok_lista_okay {
			versus_list := make([]compared_preference, 0)
			for i := 0; i < dontesi_array_len; i++ {
				compareFrom := i + 1
				if compareFrom < dontesi_array_len {
					for j := compareFrom; j < dontesi_array_len; j++ {
						x := compared_preference{
							first:      dontesi_array[i],
							second:     "¤",
							third:      dontesi_array[j],
							first_wins: true,
						}
						log.Println(x.first, "vs", x.second)
						versus_list = append(versus_list, x)
					}
				}
			}

			log.Println("Printing versus_list...")
			for i, s := range versus_list {
				log.Println(
					"index ==", i,
					"s.first ==", s.first,
					"s.second ==", s.second,
					"s.third ==", s.third,
					"s.first_wins ==", s.first_wins,
				)
			}

			log.Println("generating html...")
			local_string_builder := strings.Builder{}

			local_string_builder.WriteString(myFormStart)

			middle_html_params_to_randomize = make([]middle_html_params, 0)
			for i, s := range versus_list {
				//local_string_builder.WriteString(generate_middle_html(i, i+1, s.first, i+2, s.second))
				turn_num_string := strconv.Itoa(i)
				element_A := turn_num_string + "a"
				element_B := turn_num_string + "b"
				element_C := turn_num_string + "c"
				tempparamstruct := middle_html_params{
					radio_group_name: i,
					element_id_A:     element_A,
					choice_A_text:    s.first,
					element_id_B:     element_B,
					choice_B_text:    s.second,
					element_id_C:     element_C,
					choice_C_text:    s.third,
				}
				middle_html_params_to_randomize = append(middle_html_params_to_randomize, tempparamstruct)
			}

			rand.Seed(time.Now().UnixNano())
			rand.Shuffle(len(middle_html_params_to_randomize), func(i, j int) {
				middle_html_params_to_randomize[i], middle_html_params_to_randomize[j] = middle_html_params_to_randomize[j], middle_html_params_to_randomize[i]
			})

			for _, s := range middle_html_params_to_randomize {
				local_string_builder.WriteString(generate_middle_html(
					s.radio_group_name,
					s.element_id_A,
					s.choice_A_text,
					s.element_id_B,
					s.choice_B_text,
					s.element_id_C,
					s.choice_C_text,
				))
			}

			local_string_builder.WriteString(myFormEnd)

			log.Println(local_string_builder.String())
			the_html_code = local_string_builder.String()

			dialog.ShowInformation(
				"Siker",
				"A kérdőív HTML/CSS kódjának generálása kész!",
				main_window,
			)

			btn_start_server.Enable()
			btn_generate_survey.Disable()
		}
	})

	btn_generate_survey.Disable()

	work_indicator := widget.NewProgressBarInfinite()
	work_indicator_window := my_fyne_program.NewWindow("kérem várjon")
	work_indicator_window.Hide()
	work_indicator_window.Resize(fyne.NewSize(300, 30))
	work_indicator_window.CenterOnScreen()
	work_indicator_window.SetContent(container.NewPadded(
		container.NewGridWithRows(
			1,
			work_indicator,
		),
	))
	work_indicator_window.SetOnClosed(func() {
		my_fyne_program.Quit()
		//work_indicator_window.Hide()
		//main_window.Show()
	})

	input_list_window.SetCloseIntercept(func() {
		input_list_window.Hide()
		multiline_input.Text = dontesi_szempontok
		main_window.Show()
	})

	btn_get_dontesi_pref = widget.NewButton("1. Döntési szempontok megadása", func() {
		dialog.ShowConfirm("Döntési szempontok",
			"Itt megadhatja a döntési szempontokat",
			func(result bool) {
				if result {
					main_window.Hide()
					//work_indicator_window.Show()

					input_list_window.Show()

					// do something IN A NEW THREAD!
					//go func() {
					//	time.Sleep(20 * time.Second)
					//
					//	work_indicator.Hide()
					//	btn_generate_survey.Enable()
					//	btn_get_dontesi_pref.Disable()
					//}()

				} else {
					log.Println("no")
				}
			},
			main_window,
		)

	})

	btn_show_src := widget.NewButton(
		"Forráskód",
		func() {
			dialog.ShowConfirm(
				"Forráskód",
				"Itt majd meg lehet nézni a forráskódot",
				func(result bool) {
					if result {
						log.Println("ok")
					} else {
						log.Println("no")
					}
				},
				main_window,
			)
		},
	)

	btn_show_license := widget.NewButton(
		"A programról",
		func() {
			dialog.ShowConfirm(
				"A programról",
				"Ide kerül egy leírás a programról.",
				func(result bool) {
					if result {
						log.Println("ok")
					} else {
						log.Println("no")
					}
				},
				main_window,
			)
		},
	)

	btn_exit := widget.NewButton(
		"Kilépés",
		func() {
			dialog.ShowConfirm(
				"Kilépés",
				"Biztosan kilép?",
				func(result bool) {
					if result {
						my_fyne_program.Quit()
					} else {
						log.Println("no")
					}
				},
				main_window,
			)
		},
	)

	btn_save_settings = widget.NewButton(
		"Beállítások mentése",
		func() {
			dialog.ShowConfirm(
				"Beállítások mentése",
				"Itt lehet majd elmenteni a program beállításait.",
				func(result bool) {
					if result {
						log.Println("ok")
					} else {
						log.Println("no")
					}
				},
				main_window,
			)
		},
	)
	btn_save_settings.Disable()

	btn_load_settings = widget.NewButton(
		"Beállítások betöltése",
		func() {
			dialog.ShowConfirm(
				"Beállítások betöltése",
				"Itt lehet majd betölteni a program elmentett beállításait.",
				func(result bool) {
					if result {
						log.Println("ok")
					} else {
						log.Println("no")
					}
				},
				main_window,
			)
		},
	)

	btn_save_results = widget.NewButton(
		"Eredmények mentése",
		func() {
			dialog.ShowConfirm(
				"Eredmények mentése",
				"Itt lehet majd elmenteni a beérkezett válaszokat.",
				func(result bool) {
					if result {
						log.Println("ok")
					} else {
						log.Println("no")
					}
				},
				main_window,
			)
		},
	)
	btn_save_results.Disable()

	btn_load_results = widget.NewButton(
		"Eredmények betöltése",
		func() {
			dialog.ShowConfirm(
				"Eredmények betöltése",
				"Itt lehet majd betölteni az elmentett válaszokat.",
				func(result bool) {
					if result {
						log.Println("ok")
					} else {
						log.Println("no")
					}
				},
				main_window,
			)
		},
	)

	//myplaceholder := widget.NewSeparator()

	gui_rows := 7

	mylayout := container.NewPadded(
		container.NewGridWithRows(
			gui_rows,
			btn_get_dontesi_pref,
			btn_generate_survey,
			btn_start_server,
			btn_get_mail_addresses,
			btn_smtp_login,
			btn_send_mails,
			btn_aggregate_results,
			container.NewCenter(
				btn_save_settings,
			),
			container.NewCenter(
				btn_load_settings,
			),
			container.NewCenter(
				btn_save_results,
			),
			container.NewCenter(
				btn_load_results,
			),
			container.NewCenter(
				btn_show_src,
			),
			container.NewCenter(
				btn_show_license,
			),
			container.NewCenter(
				btn_exit,
			),
		),
	)

	main_window.SetContent(mylayout)

	main_window.ShowAndRun()
}
