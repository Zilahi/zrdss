package main

import (
	"log"
	"strconv"
	"strings"
)

func genstring(a int, b bool) string {

	ah := strconv.Itoa(a)
	bee := strconv.FormatBool(b)
	return ah + " " + bee
}

func generate_middle_html(
	radio_group_name int,
	element_id_A int,
	choice_A_text string,
	element_id_B int,
	choice_B_text string) string {

	string_builder := strings.Builder{}

	string_builder.WriteString(`
        <fieldset>
            <div class="toggle">
                <input type="radio" name="`)

	string_builder.WriteString(strconv.Itoa(radio_group_name)) //name

	string_builder.WriteString(`" value="true" id="`)

	string_builder.WriteString(strconv.Itoa(element_id_A)) //HTML element ID A

	string_builder.WriteString(`" checked>
                <label for="`)

	string_builder.WriteString(strconv.Itoa(element_id_A)) //HTML element ID A

	string_builder.WriteString(`">`)

	string_builder.WriteString(choice_A_text)

	string_builder.WriteString(`</label>
                <input type="radio" name="`)

	string_builder.WriteString(strconv.Itoa(radio_group_name)) //name

	string_builder.WriteString(`" value="false" id="`)

	string_builder.WriteString(strconv.Itoa(element_id_B)) //HTML element ID B

	string_builder.WriteString(`">
                <label for="`)

	string_builder.WriteString(strconv.Itoa(element_id_B)) //HTML element ID B

	string_builder.WriteString(`">`)

	string_builder.WriteString(choice_B_text) // Choice 2 text

	string_builder.WriteString(`</label>
            </div>
        </fieldset>`)

	return string_builder.String()
}

func main() {

	log.Println(genstring(666666666, true))

	log.Println(generate_middle_html(1, 66, "sixty six", 77, "seventy seven"))
}
