package main

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

const myFormStart = `<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Döntési preferencia kérdőív</title>

    <style>
    body, html {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        width: 100%;
        height: 100%;
        font-size: 1.4rem;
        font-family: system-ui, sans-serif;
        -webkit-font-smoothing: antialiased;
        background-color: #EEE;
    }

    fieldset {
        margin: 3.0rem;
        padding: 2rem;
        box-sizing: border-box;
        display: block;
        border: none;
        border: solid 1px #CCC;
        min-width: 0;
        background-color: #FFF;
    }

    fieldset button {
        margin: 0;
        padding: 0;
        width: 100%;
        float: left;
        display: table;
        font-size: 1.4rem;
        line-height: 140%;
        font-weight: 600;
        color: #333;
    }

    fieldset button + * {
        clear: both;
    }

    body:not(:-moz-handler-blocked) fieldset {
        display: table-cell;
    }

    .toggle {
        margin: 0rem;
        box-sizing: border-box;
        font-size: 0;
        display: flex;
        flex-flow: row nowrap;
        justify-content: flex-start;
        align-items: stretch;
    }

    .toggle input {
        width: 0;
        height: 0;
        position: absolute;
        left: -9999px;
    }

    .toggle input + label {
        margin: 0;
        padding: 0.75rem;
        box-sizing: border-box;
        position: relative;
        display: inline-block;
        border: solid 1px #DDD;
        background-color: #FFF;
        font-size: 1.4rem;
        line-height: 140%;
        font-weight: 600;
        text-align: center;
        box-shadow: 0 0 0 rgba(255, 255, 255, 0);
        transition: border-color 0.15s ease-out, color 0.25s ease-out, background-color 0.15s ease-out, box-shadow 0.15s ease-out;
        flex: 0 0 50%; display: flex; justify-content: center; align-items: center;
    }

    .toggle input + label:first-of-type {
        border-radius: 6px 0 0 6px;
        border-right: none;
    }

    .toggle input + label:last-of-type {
        border-radius: 0 6px 6px 0;
        border-left: none;
    }

    .toggle input:hover + label {
        border-color: #213140;
    }

    .toggle input:checked + label {
        background-color: #4B9DEA;
        color: #FFF;
        box-shadow: 0 0 10px rgba(102, 179, 251, 0.5);
        border-color: #4B9DEA;
        z-index: 1;
    }

    .toggle input:focus + label {
        outline: dotted 1px #CCC;
        outline-offset: 0.45rem;
    }

    @media (max-width: 800px) {
        .toggle input + label {
            padding: 0.75rem;
            flex: 0 0 50%;
            display: flex;
            justify-content: center;
            align-items: center;
        }
    }

    </style>
</head>
<body>
    <form method="POST">`

func generate_middle_html(
	radio_group_name int,
	element_id_A int,
	choice_A_text string,
	element_id_B int,
	choice_B_text string) string {

	string_builder := strings.Builder{}

	string_builder.WriteString(`
        <fieldset>
            <div class="toggle">
                <input type="radio" name="`)

	string_builder.WriteString(strconv.Itoa(radio_group_name)) //name

	string_builder.WriteString(`" value="true" id="`)

	string_builder.WriteString(strconv.Itoa(element_id_A)) //HTML element ID A

	string_builder.WriteString(`" checked>
                <label for="`)

	string_builder.WriteString(strconv.Itoa(element_id_A)) //HTML element ID A

	string_builder.WriteString(`">`)

	string_builder.WriteString(choice_A_text)

	string_builder.WriteString(`</label>
                <input type="radio" name="`)

	string_builder.WriteString(strconv.Itoa(radio_group_name)) //name

	string_builder.WriteString(`" value="false" id="`)

	string_builder.WriteString(strconv.Itoa(element_id_B)) //HTML element ID B

	string_builder.WriteString(`">
                <label for="`)

	string_builder.WriteString(strconv.Itoa(element_id_B)) //HTML element ID B

	string_builder.WriteString(`">`)

	string_builder.WriteString(choice_B_text) // Choice 2 text

	string_builder.WriteString(`</label>
            </div>
        </fieldset>`)

	return string_builder.String()
}

const myFormEnd = `
            <fieldset>
                <button>Küldés</button>
            </fieldset>
    </form>
</body>
</html>
`

func server_logic(writer http.ResponseWriter, client *http.Request) {

	if client.URL.Path != "/" {
		http.Error(writer, "404 not found.", http.StatusNotFound)
		return
	}

	switch client.Method {
	case "GET":
		writer.Header().Set("Content-Type", "text/html; charset=utf-8")

		local_string_builder := strings.Builder{}

		local_string_builder.WriteString(myFormStart)

		local_string_builder.WriteString(generate_middle_html(1, 1, "szempont 1", 2, "szempont 2"))

		local_string_builder.WriteString(generate_middle_html(2, 3, "szempont 1", 4, "szempont 3"))

		local_string_builder.WriteString(generate_middle_html(3, 5, "szempont 2", 6, "szempont 3"))

		local_string_builder.WriteString(myFormEnd)

		fmt.Fprint(writer, local_string_builder.String())
	case "POST":
		if err := client.ParseForm(); err != nil {
			fmt.Fprintf(writer, "ParseForm() err: %v", err)
			return
		}
		fmt.Fprintln(writer, "client.PostFrom =", client.PostForm)
		first := client.FormValue("1")
		second := client.FormValue("2")
		third := client.FormValue("3")
		fmt.Fprintln(writer, "first =", first)
		fmt.Fprintln(writer, "second =", second)
		fmt.Fprintln(writer, "third =", third)
	default:
		fmt.Fprintf(writer, "Sorry, only GET and POST methods are supported.")
	}

}

func send_sync(sync_channel chan bool) {
	sync_channel <- true
}

func wait_to_sync(sync_channel chan bool) {
	<-sync_channel
}

func run_server(shutdown_signaller chan bool) {

	server_address := "127.0.0.1:8080"
	server := &http.Server{
		Addr: server_address,
	}

	http.HandleFunc("/", server_logic)

	go func() {
		log.Println("Server started! Running on", server_address)
		if err := server.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
			log.Fatalf("HTTP server error: %v", err)
		}
		log.Println("Stopped serving new connections.")
	}()

	go func() {
		wait_to_sync(shutdown_signaller)

		shutdownCtx, shutdownRelease := context.WithTimeout(context.Background(), 10*time.Second)
		defer shutdownRelease()

		if err := server.Shutdown(shutdownCtx); err != nil {
			log.Fatalf("HTTP shutdown error: %v", err)
			server.Close()
		}

		log.Println("Graceful shutdown complete. Signaling success...")
		send_sync(shutdown_signaller) // bad idea to sync after possible error....
	}()

	log.Println("quitting from run_server function")
	return
}

func main() {

	server_shutdown_sync_channel := make(chan bool)

	go run_server(server_shutdown_sync_channel)

	// not needed with a GUI...
	log.Println("main thread is still alive! Waiting for exit signal...")

	console_scanner := bufio.NewScanner(os.Stdin)
	for console_scanner.Scan() {
		if console_scanner.Text() == "q" {
			log.Println("sending server shutdown signal...")
			send_sync(server_shutdown_sync_channel)

			log.Println("entering limbo... waiting for successful shutdown signal...")
			wait_to_sync(server_shutdown_sync_channel)
			log.Println("Got the successful shutdown signal! Program exits NOW!")
			break
		}
	}

}
