package main

import (
	"encoding/csv"
	"log"
	"os"
	"strconv"

	"github.com/jedib0t/go-pretty/v6/table"
)

func read_csv(filePath string) [][]string {
	f, err := os.Open(filePath)
	if err != nil {
		log.Fatal("Unable to read input file "+filePath, err)
	}
	defer f.Close()

	csvReader := csv.NewReader(f)
	records, err := csvReader.ReadAll()
	if err != nil {
		log.Fatal("Unable to parse file as CSV for "+filePath, err)
	}

	return records
}

func make_table_string(header []string, data [][]string) string {
	t := table.NewWriter()

	column_len := len(header)

	head_row := make(table.Row, column_len)
	for i, colname := range header {
		head_row[i] = colname
	}
	t.AppendHeader(head_row)

	for _, current_row := range data {

		data_row := make(table.Row, column_len)
		for j, coldata := range current_row {
			data_row[j] = coldata
		}

		t.AppendRow(data_row)
	}

	t.AppendSeparator()

	t.SetStyle(table.StyleColoredBlackOnRedWhite)

	return t.Render()
}

func main() {
	log.SetFlags(0)

	records := read_csv("./matrix.csv")

	var csv_raw_data = [][]string{}
	for _, row := range records {
		csv_raw_data = append(csv_raw_data, row)
	}

	header := csv_raw_data[0]

	var csv_data_body = [][]string{}
	for i := 1; i < len(csv_raw_data); i++ {
		csv_data_body = append(csv_data_body, csv_raw_data[i])
	}

	header = append(header, "sum")

	for i := 0; i < len(csv_data_body); i++ {

		sum := 0
		for j := 1; j < len(csv_data_body[i]); j++ {
			nuuu, err := strconv.Atoi(csv_data_body[i][j])
			if err != nil {
				log.Println("Error during conversion")
				return
			}
			sum = sum + nuuu
		}

		csv_data_body[i] = append(csv_data_body[i], strconv.Itoa(sum))
	}

	table_to_disp := make_table_string(header, csv_data_body)

	log.Println(table_to_disp)

}
