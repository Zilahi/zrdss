package main

import (
	"log"

	"github.com/jedib0t/go-pretty/v6/table"
)

func main() {
	log.SetFlags(0)

	t := table.NewWriter()

	t.AppendHeader(table.Row{"#", "A", "B", "C"})
	t.AppendRow(table.Row{"A", 1, 0, 1})
	t.AppendRow(table.Row{"B", 0, 1, 0})
	t.AppendRow(table.Row{"C", 1, 1, 1})
	t.AppendSeparator()

	log.Println(t.Render())
}
