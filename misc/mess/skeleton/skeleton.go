package main

import (
	"log"
	"os"
	"strings"

	//"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/widget"
	//"fyne.io/fyne/v2/layout"
	//"fyne.io/fyne/v2/canvas"
)

func main() {

	os.Setenv("FYNE_SCALE", "1.4")

	my_fyne_program := app.New()

	main_window := my_fyne_program.NewWindow("ZRDMH")
	main_window.SetMaster()
	main_window.Resize(fyne.NewSize(400, 500))
	main_window.CenterOnScreen()

	//readmeLabel := widget.NewLabel("Üdv a programomban!")

	var btn_get_dontesi_pref *widget.Button
	var btn_generate_survey *widget.Button
	var btn_start_server *widget.Button
	var btn_get_mail_addresses *widget.Button
	var btn_smtp_login *widget.Button
	var btn_send_mails *widget.Button
	var btn_aggregate_results *widget.Button
	var btn_save_settings *widget.Button
	var btn_load_settings *widget.Button
	var btn_save_results *widget.Button
	var btn_load_results *widget.Button

	btn_aggregate_results = widget.NewButton("7. Eredmények összesítése és kiértékelése", func() {
		dialog.ShowInformation(
			"results",
			"ezzel a gombbal kezdheti majd el az eredmények aggregáltását, és értékelését",
			main_window,
		)
	})
	btn_aggregate_results.Disable()

	btn_send_mails = widget.NewButton("6. Linkek elküldése az email címekre", func() {
		dialog.ShowInformation(
			"send emails",
			"ezzel a gombbal fogja kiküldeni az emaileket",
			main_window,
		)
		btn_aggregate_results.Enable()
		btn_send_mails.Disable()
	})
	btn_send_mails.Disable()

	btn_smtp_login = widget.NewButton("5. SMTP Bejelentkezés", func() {
		dialog.ShowInformation(
			"SMTP login",
			"itt lehet majd bejelentkezni egy email fiókba",
			main_window,
		)
		btn_send_mails.Enable()
		btn_smtp_login.Disable()
	})
	btn_smtp_login.Disable()

	btn_get_mail_addresses = widget.NewButton("4. Email címek megadása", func() {
		dialog.ShowInformation(
			"email címek listája",
			"ide írhatja majd be az email címeket",
			main_window,
		)
		btn_smtp_login.Enable()
		btn_get_mail_addresses.Disable()
	})
	btn_get_mail_addresses.Disable()

	btn_start_server = widget.NewButton("3. Szerver elindítása", func() {
		dialog.ShowInformation(
			"server start",
			"ez indítja a szervert",
			main_window,
		)
		btn_get_mail_addresses.Enable()
		btn_start_server.Disable()
	})
	btn_start_server.Disable()

	btn_generate_survey = widget.NewButton("2. Kérdőív generálása", func() {
		dialog.ShowInformation(
			"html/css",
			"itt generáljuk a kérdőívet",
			main_window,
		)
		btn_start_server.Enable()
		btn_generate_survey.Disable()
	})
	btn_generate_survey.Disable()

	work_indicator := widget.NewProgressBarInfinite()
	work_indicator_window := my_fyne_program.NewWindow("kérem várjon")
	work_indicator_window.Hide()
	work_indicator_window.Resize(fyne.NewSize(300, 30))
	work_indicator_window.CenterOnScreen()
	work_indicator_window.SetContent(container.NewPadded(
		container.NewGridWithRows(
			1,
			work_indicator,
		),
	))
	work_indicator_window.SetOnClosed(func() {
		my_fyne_program.Quit()
		//work_indicator_window.Hide()
		//main_window.Show()
	})

	window_set_dontesi_pref := my_fyne_program.NewWindow("Döntési szempontok megadása")
	window_set_dontesi_pref.Hide()
	window_set_dontesi_pref.Resize(fyne.NewSize(400, 500))
	window_set_dontesi_pref.CenterOnScreen()

	dontesi_szempontok := ""
	dontesi_array := strings.Split(dontesi_szempontok, "\n")
	dontesi_array_len := len(dontesi_array)
	dontesi_szempontok_lista_okay := false

	multiline_input := widget.NewMultiLineEntry()

	window_set_dontesi_pref.SetCloseIntercept(func() {
		window_set_dontesi_pref.Hide()
		multiline_input.Text = dontesi_szempontok
		main_window.Show()
	})

	form_set_dontesi_pref := &widget.Form{
		Items: []*widget.FormItem{},
		OnSubmit: func() {
			dontesi_szempontok = multiline_input.Text
			dontesi_array = strings.Split(dontesi_szempontok, "\n")
			dontesi_array_len = len(dontesi_array)
			dontesi_szempontok_lista_okay = (!(dontesi_szempontok == "") && dontesi_array_len > 2)

			if dontesi_szempontok_lista_okay {
				window_set_dontesi_pref.Hide()
				main_window.Show()
				btn_generate_survey.Enable()
				btn_get_dontesi_pref.Disable()
			} else {
				window_set_dontesi_pref.Show()
				dialog.ShowInformation(
					"Hiba!",
					"Kérem legalább 3 szempontot adjon meg.",
					window_set_dontesi_pref,
				)
			}
		},
	}

	multiline_input.Resize(fyne.NewSize(300, 2000))
	form_set_dontesi_pref.Append("Text", multiline_input)
	form_set_dontesi_pref.Resize(fyne.NewSize(300, 2000))

	window_set_dontesi_pref.SetContent(container.NewWithoutLayout(
		form_set_dontesi_pref,
	))

	btn_get_dontesi_pref = widget.NewButton("1. Döntési szempontok megadása", func() {
		dialog.ShowConfirm("Döntési szempontok",
			"Itt megadhatja a döntési szempontokat",
			func(result bool) {
				if result {
					main_window.Hide()
					//work_indicator_window.Show()

					window_set_dontesi_pref.Show()

					// do something IN A NEW THREAD!
					//go func() {
					//	time.Sleep(20 * time.Second)
					//
					//	work_indicator.Hide()
					//	btn_generate_survey.Enable()
					//	btn_get_dontesi_pref.Disable()
					//}()

				} else {
					log.Println("no")
				}
			},
			main_window,
		)

	})

	btn_show_src := widget.NewButton(
		"Forráskód",
		func() {
			dialog.ShowConfirm(
				"Forráskód",
				"Itt majd meg lehet nézni a forráskódot",
				func(result bool) {
					if result {
						log.Println("ok")
					} else {
						log.Println("no")
					}
				},
				main_window,
			)
		},
	)

	btn_show_license := widget.NewButton(
		"A programról",
		func() {
			dialog.ShowConfirm(
				"A programról",
				"Ide kerül egy leírás a programról.",
				func(result bool) {
					if result {
						log.Println("ok")
					} else {
						log.Println("no")
					}
				},
				main_window,
			)
		},
	)

	btn_exit := widget.NewButton(
		"Kilépés",
		func() {
			dialog.ShowConfirm(
				"Kilépés",
				"Biztosan kilép?",
				func(result bool) {
					if result {
						my_fyne_program.Quit()
					} else {
						log.Println("no")
					}
				},
				main_window,
			)
		},
	)

	btn_save_settings = widget.NewButton(
		"Beállítások mentése",
		func() {
			dialog.ShowConfirm(
				"Beállítások mentése",
				"Itt lehet majd elmenteni a program beállításait.",
				func(result bool) {
					if result {
						log.Println("ok")
					} else {
						log.Println("no")
					}
				},
				main_window,
			)
		},
	)
	btn_save_settings.Disable()

	btn_load_settings = widget.NewButton(
		"Beállítások betöltése",
		func() {
			dialog.ShowConfirm(
				"Beállítások betöltése",
				"Itt lehet majd betölteni a program elmentett beállításait.",
				func(result bool) {
					if result {
						log.Println("ok")
					} else {
						log.Println("no")
					}
				},
				main_window,
			)
		},
	)

	btn_save_results = widget.NewButton(
		"Eredmények mentése",
		func() {
			dialog.ShowConfirm(
				"Eredmények mentése",
				"Itt lehet majd elmenteni a beérkezett válaszokat.",
				func(result bool) {
					if result {
						log.Println("ok")
					} else {
						log.Println("no")
					}
				},
				main_window,
			)
		},
	)
	btn_save_results.Disable()

	btn_load_results = widget.NewButton(
		"Eredmények betöltése",
		func() {
			dialog.ShowConfirm(
				"Eredmények betöltése",
				"Itt lehet majd betölteni az elmentett válaszokat.",
				func(result bool) {
					if result {
						log.Println("ok")
					} else {
						log.Println("no")
					}
				},
				main_window,
			)
		},
	)

	//myplaceholder := widget.NewSeparator()

	gui_rows := 7

	mylayout := container.NewPadded(
		container.NewGridWithRows(
			gui_rows,
			btn_get_dontesi_pref,
			btn_generate_survey,
			btn_start_server,
			btn_get_mail_addresses,
			btn_smtp_login,
			btn_send_mails,
			btn_aggregate_results,
			container.NewCenter(
				btn_save_settings,
			),
			container.NewCenter(
				btn_load_settings,
			),
			container.NewCenter(
				btn_save_results,
			),
			container.NewCenter(
				btn_load_results,
			),
			container.NewCenter(
				btn_show_src,
			),
			container.NewCenter(
				btn_show_license,
			),
			container.NewCenter(
				btn_exit,
			),
		),
	)

	main_window.SetContent(mylayout)

	main_window.ShowAndRun()
}
