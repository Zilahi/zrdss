package main

import (
    "log"
    "time"
)

func main() {
    MyTicker1 := time.NewTicker(500 * time.Millisecond)
    MyTicker2 := time.NewTicker(1 * time.Second)

    go func() {
        for {
            <-MyTicker1.C
            log.Println("Tick Received for 500 millisecond")
        }
    }()

    go func() {
        for {
            <-MyTicker2.C
            log.Println("Tick Received 1 second")
        }
    }()

    time.Sleep(6 * time.Second)
    log.Println("Main finished")
    select {}
}
