package main

import (
	"fmt"
	"log"
	"net/http"
)

func server_logic(writer http.ResponseWriter, client *http.Request) {
	if client.URL.Path != "/" {
		http.Error(writer, "404 not found.", http.StatusNotFound)
		return
	}

	switch client.Method {
	case "GET":
		http.ServeFile(writer, client, "radiobuttons.html")
	case "POST":
		if err := client.ParseForm(); err != nil {
			fmt.Fprintf(writer, "ParseForm() err: %v", err)
			return
		}
		fmt.Fprintln(writer, "Post from website! client.PostFrom =", client.PostForm)
		first := client.FormValue("1")
		second := client.FormValue("2")
		fmt.Fprintln(writer, "first =", first)
		fmt.Fprintln(writer, "second =", second)
	default:
		fmt.Fprintf(writer, "Sorry, only GET and POST methods are supported.")
	}
}

func main() {

	server_port := ":8080"

	http.HandleFunc("/", server_logic)

	log.Println("Starting server for testing HTTP POST...")
	if err := http.ListenAndServe(server_port, nil); err != nil {
		log.Fatal(err)
	}
}
