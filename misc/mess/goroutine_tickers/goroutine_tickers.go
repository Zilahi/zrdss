package main

import (
    "fmt"
    "time"
)

func inBackground() {
    ticker := time.NewTicker(1 * time.Second)

    for _ = range ticker.C {
        fmt.Println("Ticking..")
    }
}

func main() {
    fmt.Println("Starting the ticker")

    // create a goroutine to run the ticker
    go inBackground()
    fmt.Println("After goroutine..")
    select {}
}
