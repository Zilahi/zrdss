package main

import (
	"log"
	"os"
	"strings"

	//"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/widget"
	//"fyne.io/fyne/v2/layout"
	//"fyne.io/fyne/v2/canvas"
)

func main() {

	os.Setenv("FYNE_SCALE", "1.4")

	my_fyne_program := app.New()

	main_window := my_fyne_program.NewWindow("ZRDSS")
	main_window.SetMaster()

	const main_window_width float32 = 400
	const main_window_height float32 = 500

	main_window.Resize(fyne.NewSize(main_window_width, main_window_height))
	main_window.SetFixedSize(true)

	main_window.CenterOnScreen()

	dontesi_szempontok := ""
	dontesi_array := strings.Split(dontesi_szempontok, "\n")
	dontesi_array_len := len(dontesi_array)
	dontesi_szempontok_lista_okay := false

	multiline_input := widget.NewMultiLineEntry()

	btn_accept_input := widget.NewButton("input!", func() {
		dontesi_szempontok = multiline_input.Text
		dontesi_array = strings.Split(dontesi_szempontok, "\n")
		dontesi_array_len = len(dontesi_array)
		dontesi_szempontok_lista_okay = (!(dontesi_szempontok == "") && dontesi_array_len > 2)

		if dontesi_szempontok_lista_okay {
			dialog.ShowInformation(
				"jó",
				"Minden rendben",
				main_window,
			)
			log.Println(multiline_input.Text)
		} else {
			dialog.ShowInformation(
				"Hiba!",
				"Kérem legalább 3 szempontot adjon meg.",
				main_window,
			)
		}
	})

	//mylayout := container.NewPadded(container.NewGridWithRows(
	//	2,
	//	multiline_input,
	//	container.NewCenter(btn_accept_input),
	//))

	//mylayout := container.NewVSplit(multiline_input, container.NewCenter(btn_accept_input))

	mylayout := container.NewWithoutLayout(multiline_input, btn_accept_input)

	const input_box_width = main_window_width * 0.93
	const input_box_height = main_window_height * 0.92

	const accept_btn_width = input_box_width * 0.6
	const accept_btn_height = (main_window_height - input_box_height) * 0.63

	multiline_input.Resize(fyne.NewSize(input_box_width, input_box_height))
	btn_accept_input.Resize(fyne.NewSize(accept_btn_width, accept_btn_height))

	accept_btn_pos_x := (main_window_width - btn_accept_input.Size().Width) * 0.5
	accept_btn_pos_y := main_window_height - btn_accept_input.Size().Height - ((main_window_height - input_box_height) * 0.25)

	accept_btn_pos := fyne.NewPos(accept_btn_pos_x, accept_btn_pos_y)

	input_box_pos_x := (main_window_width - multiline_input.Size().Width) * 0.33
	const input_box_pos_y float32 = 0

	input_box_pos := fyne.NewPos(input_box_pos_x, input_box_pos_y)

	btn_accept_input.Move(accept_btn_pos)
	multiline_input.Move(input_box_pos)

	main_window.SetContent(mylayout)

	main_window.ShowAndRun()
}
