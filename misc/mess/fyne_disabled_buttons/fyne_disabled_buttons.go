package main

import (
	"os"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/widget"
)

func main() {

	os.Setenv("FYNE_SCALE", "1.4")

	a := app.New()

	w1 := a.NewWindow("ZRDMH")

	w1.SetMaster()

	w1.Resize(fyne.NewSize(600, 600))

	w1.CenterOnScreen()

	readmeLabel := widget.NewLabel("Hello!")

	mybutton_to_disable := widget.NewButton("panic", func() {
		dialog.ShowInformation(
			"shittt",
			"I am not disabled!",
			w1,
		)
	})

	mybutton := widget.NewButton("disable", func() {
		mybutton_to_disable.Disable()
		dialog.ShowInformation(
			"cool",
			"mybutton pressed\nmybutton_to_disable is disabled!",
			w1,
		)
	})

	enabler := widget.NewButton("enable", func() {
		mybutton_to_disable.Enable()
		dialog.ShowInformation(
			"cool",
			"enabler pressed\nmybutton_to_disable is enabled!",
			w1,
		)
	})

	separator := widget.NewSeparator()

	/*
		layout := container.NewVBox(
			readmeLabel,
			mybutton,
			mybutton_to_disable,
			enabler,
		)
	*/

	betterlayout := container.NewPadded(
		container.NewCenter(
			container.NewVBox(
				readmeLabel,
				separator,
				container.NewHBox(
					mybutton,
					separator,
					mybutton_to_disable,
				),
				separator,
				enabler,
			),
		),
	)

	w1.SetContent(betterlayout)

	w1.ShowAndRun()
}
