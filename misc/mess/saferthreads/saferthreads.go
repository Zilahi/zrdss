package main

import (
	"log"
	"sync"
	"time"
)

type future struct {
	safety   sync.Mutex
	finished chan bool
}

func (my_obj *future) send(run_this func()) {
	go func() {
		my_obj.safety.Lock()
		run_this()
		my_obj.safety.Unlock()

		my_obj.finished <- true
	}()
}

func (my_obj *future) wait_here(n int) {
	for i := 0; i < n; i++ {
		<-my_obj.finished
	}
}

func newFuture() future {
	return future{finished: make(chan bool)}
}

func main() {

	log.Println("init")
	n := 6
	wish := newFuture()
	scramble := 0

	log.Println("sending")
	for i := 0; i < n; i++ {
		wish.send(func() {
			time.Sleep(1 * time.Second)
			scramble++
		})
	}

	log.Println("waiting")
	wish.wait_here(n)

	log.Println("done")
	log.Println(scramble)

}
