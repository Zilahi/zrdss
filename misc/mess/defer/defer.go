package main

import (
	"log"
)

func lifo_defer_order() {
	defer log.Println("Third")
	defer log.Println("Second")
	defer log.Println("First")
}

func main() {
	lifo_defer_order()
}
