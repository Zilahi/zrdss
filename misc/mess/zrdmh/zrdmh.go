package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/widget"
)

const myFormStart = `<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Döntési preferencia kérdőív</title>

    <style>
    body, html {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        width: 100%;
        height: 100%;
        font-size: 1.4rem;
        font-family: system-ui, sans-serif;
        -webkit-font-smoothing: antialiased;
        background-color: #EEE;
    }

    fieldset {
        margin: 3.0rem;
        padding: 2rem;
        box-sizing: border-box;
        display: block;
        border: none;
        border: solid 1px #CCC;
        min-width: 0;
        background-color: #FFF;
    }

    fieldset button {
        margin: 0;
        padding: 0;
        width: 100%;
        float: left;
        display: table;
        font-size: 1.4rem;
        line-height: 140%;
        font-weight: 600;
        color: #333;
    }

    fieldset button + * {
        clear: both;
    }

    body:not(:-moz-handler-blocked) fieldset {
        display: table-cell;
    }

    .toggle {
        margin: 0rem;
        box-sizing: border-box;
        font-size: 0;
        display: flex;
        flex-flow: row nowrap;
        justify-content: flex-start;
        align-items: stretch;
    }

    .toggle input {
        width: 0;
        height: 0;
        position: absolute;
        left: -9999px;
    }

    .toggle input + label {
        margin: 0;
        padding: 0.75rem;
        box-sizing: border-box;
        position: relative;
        display: inline-block;
        border: solid 1px #DDD;
        background-color: #FFF;
        font-size: 1.4rem;
        line-height: 140%;
        font-weight: 600;
        text-align: center;
        box-shadow: 0 0 0 rgba(255, 255, 255, 0);
        transition: border-color 0.15s ease-out, color 0.25s ease-out, background-color 0.15s ease-out, box-shadow 0.15s ease-out;
        flex: 0 0 50%; display: flex; justify-content: center; align-items: center;
    }

    .toggle input + label:first-of-type {
        border-radius: 6px 0 0 6px;
        border-right: none;
    }

    .toggle input + label:last-of-type {
        border-radius: 0 6px 6px 0;
        border-left: none;
    }

    .toggle input:hover + label {
        border-color: #213140;
    }

    .toggle input:checked + label {
        background-color: #4B9DEA;
        color: #FFF;
        box-shadow: 0 0 10px rgba(102, 179, 251, 0.5);
        border-color: #4B9DEA;
        z-index: 1;
    }

    .toggle input:focus + label {
        outline: dotted 1px #CCC;
        outline-offset: 0.45rem;
    }

    @media (max-width: 800px) {
        .toggle input + label {
            padding: 0.75rem;
            flex: 0 0 50%;
            display: flex;
            justify-content: center;
            align-items: center;
        }
    }

    </style>
</head>
<body>
    <form method="POST">`

func generate_middle_html(
	radio_group_name int,
	element_id_A int,
	choice_A_text string,
	element_id_B int,
	choice_B_text string) string {

	string_builder := strings.Builder{}

	string_builder.WriteString(`
        <fieldset>
            <div class="toggle">
                <input type="radio" name="`)

	string_builder.WriteString(strconv.Itoa(radio_group_name)) //name

	string_builder.WriteString(`" value="true" id="`)

	string_builder.WriteString(strconv.Itoa(element_id_A)) //HTML element ID A

	string_builder.WriteString(`" checked>
                <label for="`)

	string_builder.WriteString(strconv.Itoa(element_id_A)) //HTML element ID A

	string_builder.WriteString(`">`)

	string_builder.WriteString(choice_A_text)

	string_builder.WriteString(`</label>
                <input type="radio" name="`)

	string_builder.WriteString(strconv.Itoa(radio_group_name)) //name

	string_builder.WriteString(`" value="false" id="`)

	string_builder.WriteString(strconv.Itoa(element_id_B)) //HTML element ID B

	string_builder.WriteString(`">
                <label for="`)

	string_builder.WriteString(strconv.Itoa(element_id_B)) //HTML element ID B

	string_builder.WriteString(`">`)

	string_builder.WriteString(choice_B_text) // Choice 2 text

	string_builder.WriteString(`</label>
            </div>
        </fieldset>`)

	return string_builder.String()
}

const myFormEnd = `
            <fieldset>
                <button>Küldés</button>
            </fieldset>
    </form>
</body>
</html>
`

var the_html_code string

func server_logic(writer http.ResponseWriter, client *http.Request) {

	if client.URL.Path != "/" {
		http.Error(writer, "404 not found.", http.StatusNotFound)
		return
	}

	switch client.Method {
	case "GET":
		writer.Header().Set("Content-Type", "text/html; charset=utf-8")
		fmt.Fprint(writer, the_html_code)
	case "POST":
		if err := client.ParseForm(); err != nil {
			fmt.Fprintf(writer, "ParseForm() err: %v", err)
			return
		}
		fmt.Fprintln(writer, "client.PostFrom =", client.PostForm)
		//first := client.FormValue("1")
		//second := client.FormValue("2")
		//third := client.FormValue("3")
		//fmt.Fprintln(writer, "first =", first)
		//fmt.Fprintln(writer, "second =", second)
		//fmt.Fprintln(writer, "third =", third)
	default:
		fmt.Fprintf(writer, "Sorry, only GET and POST methods are supported.")
	}

}

func send_sync(sync_channel chan bool) {
	sync_channel <- true
}

func wait_to_sync(sync_channel chan bool) {
	<-sync_channel
}

const server_address = "127.0.0.1:8080"

var server = &http.Server{
	Addr: server_address,
}

func run_server(shutdown_signaller chan bool) {

	// http.HandleFunc("/", server_logic)

	go func() {
		log.Println("Server started! Running on", server_address)
		if err := server.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
			log.Fatalf("HTTP server error: %v", err)
		}
		log.Println("Stopped serving new connections.")
	}()

	go func() {
		wait_to_sync(shutdown_signaller)

		shutdownCtx, shutdownRelease := context.WithTimeout(context.Background(), 3*time.Second)
		defer shutdownRelease()

		if err := server.Shutdown(shutdownCtx); err != nil {
			log.Fatalf("HTTP shutdown error: %v", err)
			server.Close()
		}

		log.Println("Graceful shutdown complete. Signaling success...")
		send_sync(shutdown_signaller)
		// bad idea to sync after possible error....
	}()

	log.Println("quitting from run_server function")
	return
}

type compared_preference struct {
	first      string
	second     string
	first_wins bool
}

func main() {

	//generated_full_html := ""

	a := app.New()

	http.HandleFunc("/", server_logic)

	server_shutdown_sync_channel := make(chan bool)

	server_already_running := false

	os.Setenv("FYNE_SCALE", "1.4")

	w1 := a.NewWindow("ZRDMH")

	w1.SetMaster()

	w1.CenterOnScreen()

	w1.SetCloseIntercept(func() {
		dialog.ShowConfirm("Program bezárása", "Biztosan kilép a programból?",
			func(response bool) {
				if response {
					w1.Close()
				}
			}, w1)
	})

	// Declare golang multiline string
	readmeText0 := `
Üdv Zilahi Richard döntéstámogató programjában!

Mivel valami nevet kellett adni a projektnek, ezért:
ZRDMH == Zilahi Richard's Decision Making Helper

Viszont a program jelenleg fejlesztés alatt áll, vagyis nincs kész...

Arra gondoltam, hogy ez lesz a csontváza a szerver oldali programot irányító
admin kezelőfelületnek...

A használathoz, egymás után sorban kell majd kattintani a következő gombokra:
`

	readmeLabel := widget.NewLabel(readmeText0)

	w2 := a.NewWindow("Döntési szempontok megadása")

	dontesi_szempontok := ""
	dontesi_array := strings.Split(dontesi_szempontok, "\n")
	dontesi_array_len := len(dontesi_array)
	dontesi_szempontok_lista_okay := false
	versus_list := make([]compared_preference, 0)

	textArea1 := widget.NewMultiLineEntry()

	w2.CenterOnScreen()

	w2.SetCloseIntercept(func() {
		w2.Hide()
		textArea1.Text = dontesi_szempontok
		w1.Show()
	})

	readmeText1 := `
Kérem adja meg a vizsgálni kívánt döntési szempontokat.
Minden sorba egy szempontot írjon.
Kérem legalább 2 szempontot adjon meg.
Ha kész, akkor kattintson a "Kész" gombra.
`

	form := &widget.Form{
		Items: []*widget.FormItem{},
		OnSubmit: func() {
			dontesi_szempontok = textArea1.Text
			dontesi_array = strings.Split(dontesi_szempontok, "\n")
			dontesi_array_len = len(dontesi_array)
			dontesi_szempontok_lista_okay = (!(dontesi_szempontok == "") && len(dontesi_array) > 1)

			if dontesi_szempontok_lista_okay {
				w2.Hide()
				w1.Show()
			} else {
				w2.Show()
				dialog.ShowInformation(
					"Hiba!",
					"Kérem legalább 2 szempontot adjon meg.",
					w2,
				)
			}
		},
	}

	form.Append("Text", textArea1)

	w2.SetContent(container.NewVBox(
		widget.NewLabel(readmeText1),
		form,
	))

	w1.SetContent(container.NewVBox(
		readmeLabel,
		widget.NewButton("1. Döntési szempontok megadása", func() {
			w1.Hide()
			w2.Show()
		}),
		widget.NewButton("2. Kérdőív generálása", func() {

			log.Println("dontesi_szempontok is okay == ", dontesi_szempontok_lista_okay)

			if dontesi_szempontok_lista_okay {
				for i := 0; i < dontesi_array_len; i++ {
					compareFrom := i + 1
					if compareFrom < dontesi_array_len {
						for j := compareFrom; j < dontesi_array_len; j++ {
							x := compared_preference{
								first:      dontesi_array[i],
								second:     dontesi_array[j],
								first_wins: true,
							}
							log.Println(x.first, "vs", x.second)
							versus_list = append(versus_list, x)
						}
					}
				}

				log.Println("Printing versus_list...")
				for i, s := range versus_list {
					log.Println(
						"index ==", i,
						"s.first ==", s.first,
						"s.second ==", s.second,
						"s.first_wins ==", s.first_wins,
					)
				}

				log.Println("generating html...")
				local_string_builder := strings.Builder{}

				local_string_builder.WriteString(myFormStart)

				for i, s := range versus_list {

					local_string_builder.WriteString(generate_middle_html(i, i+1, s.first, i+2, s.second))
				}

				local_string_builder.WriteString(myFormEnd)

				log.Println(local_string_builder.String())
				the_html_code = local_string_builder.String()

				dialog.ShowInformation(
					"Siker",
					"A kérdőív HTML/CSS kódjának generálása kész!",
					w1,
				)
			}
		}),
		widget.NewButton("3. Szerver elindítása", func() {

			if server_already_running {
				log.Println("sending server shutdown signal...")
				send_sync(server_shutdown_sync_channel)
				wait_to_sync(server_shutdown_sync_channel)
				server_already_running = false
				dialog.ShowInformation(
					"Infó",
					"A szerver már futott, ezért most leállítottuk.",
					w1,
				)
			}

			if server_already_running == false {
				if the_html_code != "" {
					log.Println("there is html code for the server....")

					go run_server(server_shutdown_sync_channel)

					dialog.ShowInformation(
						"Infó",
						"A szerver elindult.\nA kérdőív megtekinthető a http://127.0.0.1:8080 címen.",
						w1,
					)

				} else {
					dialog.ShowInformation(
						"Hiba!",
						"Nincs html kód generálva!",
						w1,
					)
				}
			}

			/*
				dialog.ShowInformation(
					"Hiba!",
					"Ez még nincs kész!",
					w1,
				)
			*/
		}),
		widget.NewButton("4. Email címek megadása", func() {
			dialog.ShowInformation(
				"Hiba!",
				"Ez még nincs kész!",
				w1,
			)
		}),
		widget.NewButton("5. SMTP Bejelentkezés", func() {
			dialog.ShowInformation(
				"Hiba!",
				"Ez még nincs kész!",
				w1,
			)
		}),
		widget.NewButton("6. Linkek elküldése az email címekre", func() {
			dialog.ShowInformation(
				"Hiba!",
				"Ez még nincs kész!",
				w1,
			)
		}),
		widget.NewButton("7. Eredmények összesítése és kiértékelése", func() {
			dialog.ShowInformation(
				"Hiba!",
				"Ez még nincs kész!",
				w1,
			)
		}),
	))

	w1.Show()

	log.Println("GUI CODE STARTS TO RUN")

	a.Run()

	log.Println("END OF MAIN")
}
