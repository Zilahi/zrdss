package main

import (
    "fyne.io/fyne/v2/app"
    "fyne.io/fyne/v2/container"
    "fyne.io/fyne/v2/widget"
)

func main() {
    a := app.New()
    w := a.NewWindow("ZRDMH")

    readmeLabel := widget.NewLabel("Üdv Zilahi Richárd döntéstámogató programjában!\nA program jelenleg fejlesztés alatt áll.")

    w.SetContent(container.NewVBox(
        readmeLabel,
        widget.NewButton("Hi!", func() {
            readmeLabel.SetText("Welcome :)")
        }),
    ))

    w.ShowAndRun()
}
