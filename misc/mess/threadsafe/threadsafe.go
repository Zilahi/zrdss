package main

import (
	"log"
	"sync"
	//"time"
)

var end_signals = make(chan bool) // shit happens if we put it inside of our struct...
type Threadsafe struct {
	lock    sync.Mutex
	safeint int
}

func (my_obj *Threadsafe) access(start_this func()) {
	defer func() {
		go func() {
			end_signals <- true
		}()
	}()
	defer my_obj.lock.Unlock()

	my_obj.lock.Lock()
	start_this()
}

func (my_obj *Threadsafe) waitfor(n int) {
	for i := 0; i < n; i++ {
		<-end_signals
	}
}

func main() {

	safe_access := Threadsafe{safeint: 0}

	const n = 8000

	for i := 0; i < n; i++ {
		go safe_access.access(func() {
			safe_access.safeint++
		})
	}

	safe_access.waitfor(n)

	close(end_signals)

	log.Println(safe_access.safeint)

}
