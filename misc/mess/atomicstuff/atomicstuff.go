package main

import (
	"bufio"
	//"context"
	//"errors"
	//"fmt"
	"log"
	//"net/http"
	"os"
	//"strconv"
	//"strings"
	//"time"
	//"sync"
	"sync/atomic"
	//"time"
)

var rwchannel chan bool = make(chan bool)
var initchannel chan bool = make(chan bool)

var endchannel chan bool = make(chan bool)

var hot_atom atomic.Value

var cold_atom atomic.Value

type my_custom_type struct {
	a int
	b bool
}

func syncinitstart() {
	initchannel <- true
}

func syncinitend() {
	<-initchannel
}

func writesync() {
	rwchannel <- true
}

func readsync() {
	<-rwchannel
}

func write() {
	defer syncinitstart()

	go func() {
		defer writesync()
		syncinitend()

		hot_atom.Store(cold_atom.Load().(my_custom_type))
	}()
}

func reader() {
	defer syncinitstart()

	go func() {
		defer readsync()
		syncinitend()

		for {
			select {
			case <-endchannel:
				break
			case <-rwchannel:
				log.Println(hot_atom.Load().(my_custom_type))
			}
		}

	}()
}

func main() {

	reader()

	cold_atom.Store(my_custom_type{100, true})
	write() // wg +1 complete

	cold_atom.Store(my_custom_type{800, false})
	write() // wg +1 complete

	cold_atom.Store(my_custom_type{2000, true})
	write() // wg +1 complete

	log.Println("main thread is still alive! Waiting for exit signal...")
	console_scanner := bufio.NewScanner(os.Stdin)
	for console_scanner.Scan() {
		if console_scanner.Text() == "q" {
			log.Println("Got the shutdown signal!")
			log.Println("Program exits NOW")
			break
		}
	}

}
