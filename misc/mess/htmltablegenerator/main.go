package main

import (
	"log"

	"github.com/jedib0t/go-pretty/v6/table"
)

const fancythmltablestart = `<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tábla</title>
    <style>
html,
body {
  height: 100%;
}
body {
  margin: 0;
  background: linear-gradient(45deg, #49a09d, #5f2c82);
  font-family: sans-serif;
  font-weight: 100;
}
.container {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}
table {
  width: 800px;
  border-collapse: collapse;
  overflow: hidden;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
}
th,
td {
  padding: 15px;
  background-color: rgba(255, 255, 255, 0.2);
  color: #fff;
}
th {
  text-align: left;
}
thead th {
  background-color: #55608f;
}
tbody tr:hover {
  background-color: rgba(255, 255, 255, 0.3);
}
tbody td {
  position: relative;
}
tbody td:hover:before {
  content: "";
  position: absolute;
  left: 0;
  right: 0;
  top: -9999px;
  bottom: -9999px;
  background-color: rgba(255, 255, 255, 0.2);
  z-index: -1;
}

    </style>
</head>
<body>`

const fancythmltableend = `</body>
</html>`

func main() {
	log.SetFlags(0)

	t := table.NewWriter()

	t.AppendHeader(table.Row{"#", "E1", "E2", "E3", "E4", "E5"})
	t.AppendRow(table.Row{"E1", 0, 0, 1, 1, 0})
	t.AppendRow(table.Row{"E2", 1, 0, 1, 1, 1})
	t.AppendRow(table.Row{"E3", 0, 0, 0, 0, 0})
	t.AppendRow(table.Row{"E4", 0, 0, 1, 0, 0})
	t.AppendRow(table.Row{"E5", 1, 0, 1, 1, 0})
	t.AppendSeparator()

	t.SetStyle(table.StyleColoredBlackOnRedWhite)

	t.Style().HTML = table.HTMLOptions{
		CSSClass:    "mytable",
		EmptyColumn: "&nbsp;",
		EscapeText:  true,
		Newline:     "<br/>",
	}
	rendered_html_table := t.RenderHTML()

	okay_html_page := fancythmltablestart + rendered_html_table + fancythmltableend

	log.Println(okay_html_page)
}
